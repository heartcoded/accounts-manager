package com.diana.springboot.accountsmanager.controller;


import com.diana.springboot.accountsmanager.service.IAccountManager;
import com.diana.springboot.accountsmanager.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AccountsManagerController {

    @Autowired
    IAccountManager accountManager;

    @GetMapping("/accounts")
    public List<Account> getAccounts(){
       return accountManager.getAccounts();
    }

    @GetMapping("/accounts/{iban}")
    public Account getAccountById(@PathVariable Long iban){
        return accountManager.getAccountByIban(iban);
    }

    @PutMapping("/accounts-withdraw/{iban}/{amount}")
    public double withdraw(@PathVariable Long iban, @PathVariable Double amount){

        Account existingAccount = accountManager.getAccountByIban(iban);
        return accountManager.withdraw(existingAccount,amount);
    }

    @PutMapping("/accounts-deposit/{iban}/{amount}")
    public double deposit(@PathVariable Long iban, @PathVariable Double amount){

        Account existingAccount = accountManager.getAccountByIban(iban);
        return accountManager.deposit(existingAccount,amount);
    }
}
