package com.diana.springboot.accountsmanager.repo;

import com.diana.springboot.accountsmanager.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountsRepository extends JpaRepository<Account,Long> {
}
