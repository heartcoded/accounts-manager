package com.diana.springboot.accountsmanager.exception;

public class AccountNotFound extends RuntimeException {
    public AccountNotFound(String message){
        super(message);
    }
}
