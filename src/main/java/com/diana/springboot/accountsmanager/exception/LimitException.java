package com.diana.springboot.accountsmanager.exception;

public class LimitException extends RuntimeException {

    public LimitException(String message){
        super(message);
    }
}
