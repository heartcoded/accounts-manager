package com.diana.springboot.accountsmanager.exception;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ExceptionResponse {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date timestamp;
    private String message;
    private String extraInfo;

    public ExceptionResponse() {

    }

    public ExceptionResponse(Date timestamp, String message, String extraInfo) {
        this.timestamp = timestamp;
        this.message = message;
        this.extraInfo = extraInfo;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    @Override
    public String toString() {
        return "ExceptionResponse [timestamp=" + timestamp + ", message=" + message + ", extraInfo=" + extraInfo + "]";
    }

}
