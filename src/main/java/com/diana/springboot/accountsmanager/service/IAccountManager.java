package com.diana.springboot.accountsmanager.service;

import com.diana.springboot.accountsmanager.exception.LimitException;
import com.diana.springboot.accountsmanager.exception.NotEnoughFundsException;
import com.diana.springboot.accountsmanager.model.Account;

import java.util.List;

public interface IAccountManager {

    Account getAccountByIban(Long iban);
    List<Account> getAccounts();
    double getBalance(Account account);
    double deposit(Account account, double amount);
    double withdraw(Account account, double amount);
}
