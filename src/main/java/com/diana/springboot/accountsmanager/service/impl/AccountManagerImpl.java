package com.diana.springboot.accountsmanager.service.impl;

import com.diana.springboot.accountsmanager.exception.AccountNotFound;
import com.diana.springboot.accountsmanager.exception.LimitException;
import com.diana.springboot.accountsmanager.exception.NotEnoughFundsException;
import com.diana.springboot.accountsmanager.model.Account;
import com.diana.springboot.accountsmanager.repo.AccountsRepository;
import com.diana.springboot.accountsmanager.service.IAccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class AccountManagerImpl implements IAccountManager {
    @Autowired
    private AccountsRepository accountsRepository;

    @Override
    public double getBalance(Account account) {
        if (account == null) {
            throw new IllegalArgumentException("Invalid account");
        }
        return account.getBalance();
    }

    @Override
    public double deposit(Account account, double amount) {

        if (account != null && amount >=0) {
            account.setBalance(account.getBalance() + amount);
            accountsRepository.save(account);
            return getBalance(account);
        } else {
            throw new IllegalArgumentException("Invalid args");
        }
    }

    @Override
    public double withdraw(Account account, double amount){

        if (account == null || amount < 0 ) {
            throw new IllegalArgumentException("Invalid args");
        }
        if (amount > account.getBalance()) {
            throw new NotEnoughFundsException("Not enough funds for iban " + account.getIban());
        }
        if (amount > account.getWithdrawalLimit()) {
            throw new LimitException("Withdrawal limit for iban " + account.getIban() + " is " + account.getWithdrawalLimit());
        }
        account.setBalance(account.getBalance() - amount);
        accountsRepository.save(account);
        return getBalance(account);

    }

    @Override
    public Account getAccountByIban(Long iban) {
        Optional<Account> account = accountsRepository.findById(iban);
        if(!account.isPresent()){
            throw new AccountNotFound("Account with iban "+iban+" not found");
        }
        return account.get();
    }

    @Override
    public List<Account> getAccounts() {
       return accountsRepository.findAll();
    }
}
