package com.diana.springboot.accountsmanager;

import com.diana.springboot.accountsmanager.model.Account;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class AccountsManagerApplication {

	public static void main(String[] args) throws Exception{
		SpringApplication.run(AccountsManagerApplication.class, args);
	}

}
