package com.diana.springboot.accountsmanager.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ACCOUNTS")
public class Account {

    @Id
    private Long iban;
    private double balance;
    @Column(name="WITHDRAWAL_LIMIT")
    private double withdrawalLimit;

    public Account() {
        this.balance = 0;
        this.withdrawalLimit = 0;
    }

    public Account(Long iban, double balance, double withdrawalLimit) {
        this.iban = iban;
        this.balance = balance;
        this.withdrawalLimit = withdrawalLimit;
    }

    public Long getIban() {
        return iban;
    }

    public void setIban(Long iban) {
        this.iban = iban;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getWithdrawalLimit() {
        return withdrawalLimit;
    }

    public void setWithdrawalLimit(double withdrawalLimit) {
        this.withdrawalLimit = withdrawalLimit;
    }

    @Override
    public String toString() {
        return "Account{" +
                "iban=" + iban +
                ", balance=" + balance +
                ", withdrawalLimit=" + withdrawalLimit +
                '}';
    }
}
