The most used widely used annotation in Mockito is @Mock. We can use @Mock to create and inject mocked instances without having to call Mockito.mock manually.


once creaetd a mock will remember ALL interactions (all methods applied to it) then you can selectively verify whatever interactions you are interested in


Mockito.verify() : verifies that the methods you called on your mocked object are indeed called. If they weren't called,
or called with the wrong parameters, or called the wrong number of times, they would fail your test.



!!!
Mockito JUnit Runner keeps tests clean and improves debugging experience. 
MockitoJUnitRunner gives you automatic validation of framework usage, as well as an automatic initMocks().
So I would recommend the use of the MockitoJUnitRunner wherever possible. However, as Tomasz Nurkiewicz has correctly pointed out,
you can't use it if you need another JUnit runner, such as the Spring one.

My recommendation has now changed. The Mockito team have added a new feature since I first wrote this answer.
 It's a JUnit rule, which performs exactly the same function as the MockitoJUnitRunner. But it's better, because it doesn't preclude the use of other runners.

Include

@Rule 
public MockitoRule rule = MockitoJUnit.rule();
in your test class. This initialises the mocks, and automates the framework validation; 
just like MockitoJUnitRunner does. But now, you can use SpringJUnit4ClassRunner or any other JUnitRunner as well.
From Mockito 2.1.0 onwards, there are additional options that control exactly what kind of problems get reported.
!!!


When Mockito creates a mock – it does so from the Class of a Type, not from an actual instance.
The mock simply creates a bare-bones shell instance of the Class, 
entirely instrumented to track interactions with it.

On the other hand, the spy will wrap an existing instance. It will still behave in the same way as the normal instance
– the only difference is that it will also be instrumented to track all the interactions with it.


ArgumentCaptor class is used to capture argument values for further assertions. Mockito verifies argument values in natural java style: by using an equals() method.	

@InjectMocks - injects mock or spy fields into tested object automatically.

-----------------------------------
Interface Answer<T>
As the Answer interface has just one method it is already possible to implement it in Java 8 using a lambda expression for very simple situations.

Generic interface to be used for configuring mock's answer. Answer specifies an action that is executed 
and a return value that is returned when you interact with the mock.

-------------------------------------
Mockito now offers an Incubating, optional support for mocking final classes and methods. 
This is a fantastic improvement that demonstrates Mockito's everlasting quest for improving testing experience. 
Our ambition is that Mockito "just works" with final classes and methods. 
Previously they were considered unmockable, preventing the user from mocking. 

-----------------------------------

As you write a test that doesn't need any dependencies from the Spring Boot container, the classic/plain Mockito is the way to follow : 
it is fast and favors the isolation of the tested component.
If your test needs to rely on the Spring Boot container and you want also to add or mock one of the container beans : @MockBean from Spring Boot is the way.

-----------------------------------

Hamcrest is a framework for writing matcher objects allowing ‘match’ rules to be defined declaratively. 
There are a number of situations where matchers are invaluble, such as UI validation, or data filtering, 
but it is in the area of writing flexible tests that matchers are most commonly used.
















