package com.diana.springboot.accountsmanager.service;

import com.diana.springboot.accountsmanager.exception.AccountNotFound;
import com.diana.springboot.accountsmanager.exception.LimitException;
import com.diana.springboot.accountsmanager.exception.NotEnoughFundsException;
import com.diana.springboot.accountsmanager.model.Account;
import com.diana.springboot.accountsmanager.repo.AccountsRepository;
import com.diana.springboot.accountsmanager.service.impl.AccountManagerImpl;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.mockito.Mockito.*;

public class AccountManagerImplTest {


    @InjectMocks
    private AccountManagerImpl accountManager;
    @Mock
    private AccountsRepository accountsRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getBalanceGoodTest() {
        Account a1 = new Account();
        a1.setBalance(200);
        assertEquals(200, accountManager.getBalance(a1), 0);
    }


    @Test(expected = IllegalArgumentException.class)
    public void getBalanceExceptionTest() {
     accountManager.getBalance(null);

    }

    @Test
    public void depositGoodTest(){
        Account a1 = new Account();
        double amount  =  600;
        a1.setBalance(150);
        //daca nu il mockuies si as ignora linia 50, mi-ar vedea obiectul null si ar crapa
        when(accountsRepository.save(any(Account.class))).thenReturn(new Account());
        assertEquals(750,accountManager.deposit(a1,amount),0.01);
    }


    @Test(expected = IllegalArgumentException.class)
    public void depositNegativeAmountTest(){
        Account a1 = new Account();
        double amount  =  -600;
        a1.setBalance(150);
        accountManager.deposit(a1,amount);

    }

    @Test(expected = IllegalArgumentException.class)
    public void depositNullAccountTest(){
        accountManager.deposit(null,100);
    }

    @Test
    public void withdrawGoodTest(){
        Account a1 = new Account();
        double amount = 20;
        a1.setBalance(500);
        a1.setWithdrawalLimit(100);
        when(accountsRepository.save(any(Account.class))).thenReturn(new Account());
        assertEquals(480,accountManager.withdraw(a1,amount ),0.1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void withdrawInvalidArgsTest(){
        Account a1 = new Account();
        double amount = -20;
        accountManager.withdraw(a1,amount);
    }
    @Test(expected = IllegalArgumentException.class)
    public void withdrawInvalidArgs2Test(){
        double amount = 20;
        accountManager.withdraw(null,amount);
    }
    @Test(expected = NotEnoughFundsException.class)
    public void withdrawNotEnoughFundsTest(){
        Account a1 = new Account();
        a1.setBalance(10);
        double amount = 11;
        accountManager.withdraw(a1,amount);
    }

    @Test(expected = LimitException.class)
    public void withdrawLimitExceptionTest(){
        Account a1 = new Account();
        a1.setBalance(190);
        double amount = 110;
        a1.setWithdrawalLimit(100);
        accountManager.withdraw(a1,amount);
    }

    @Test
    public void getAccountByIbanGoodTest(){

        Optional<Account> mockedAccount = mock(Optional.class);
        when(accountsRepository.findById(any(Long.class))).thenReturn(mockedAccount);
        when(mockedAccount.isPresent()).thenReturn(true);
        accountManager.getAccountByIban(123L);
        verify(mockedAccount).get();
    }

    @Test(expected = AccountNotFound.class)
    public void getAccountByIbanBadTest(){

        Optional<Account> mockedAccount = mock(Optional.class);
        when(accountsRepository.findById(any(Long.class))).thenReturn(mockedAccount);
        when(mockedAccount.isPresent()).thenReturn(false);
        accountManager.getAccountByIban(123L);

        //nu ajunge sa se apeleze
      //  verify(mockedAccount,times(0)).get();
    }

}
/*
@Override
    public Account getAccountByIban(Long iban) {
        Optional<Account> account = accountsRepository.findById(iban);
        if(!account.isPresent()){
            throw new AccountNotFound("Account with iban "+iban+" not found");
        }
        return account.get();
    }
 */

