package com.diana.springboot.accountsmanager.learning;

import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ArgumentCaptorTests {


	@Mock
	List mockedList;
	@Captor
	ArgumentCaptor captor;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void verifyCalledMethodsTest() {
		mockedList.add("Java Code Geeks");
		mockedList.add("Mockito");
		verify(mockedList,times(2)).add(captor.capture());
		List<String> values = captor.getAllValues();
		assertEquals("Java Code Geeks", values.get(0));
		assertEquals("Mockito", values.get(1));
	}
}
