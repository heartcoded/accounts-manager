package com.diana.springboot.accountsmanager.learning;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class MockitoStubbingTests {


	@Mock
	LinkedList mockedList;
	@Spy
	List<String> spiedList = new ArrayList<>();


	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void test1(){
		//stubbing
		when(mockedList.get(0)).thenReturn("first");
		when(mockedList.get(1)).thenThrow(new RuntimeException());

		//following prints "first"
		System.out.println(mockedList.get(0));

		//following throws runtime exception
	//	System.out.println(mockedList.get(1));

		//following prints "null" because get(999) was not stubbed
		System.out.println(mockedList.get(999));

	    verify(mockedList).get(0);

	}

	@Test
	public void argumentMatchersTest(){
		//stubbing using built-in anyInt() argument matcher
		when(mockedList.get(anyInt())).thenReturn("element");

		assertEquals("element", mockedList.get(0));
		assertEquals("element", mockedList.get(24));
		//following prints "element"
		System.out.println(mockedList.get(999));

		//you can also verify using an argument matcher
		verify(mockedList,times(3)).get(anyInt());
	}
	@Test
	public void callingMethodOnAMockTest() {
		mockedList.add("one");
		verify(mockedList).add("one");
		System.out.println("size of mockedList without mocking size() method: "+mockedList.size());
		// e 0 pentru ca lista noastra e un mock -> nu e un obiect real, deci add nu face nimic, deci nu incrementeaza size-ul
		assertEquals(0, mockedList.size());

		//aici ii spunem mockului explicit ca atunci cand apelam size() sa returneze 100
		when(mockedList.size()).thenReturn(100);
		System.out.println("size of mockedList after mocking size() method: "+mockedList.size());
		assertEquals(100, mockedList.size());
	}


	@Test
	public void callingMethodOnASpyTest() {
		spiedList.add("one");
		spiedList.add("two");

		verify(spiedList).add("one");
		verify(spiedList).add("two");

		//spy -> un obiect real ;add face ceva deci mareste size-ul
		assertEquals(2, spiedList.size());

		doReturn(100).when(spiedList).size();
		assertEquals(100, spiedList.size());
	}

	@Test
	public void mockingMethodsWhenUsingSpiesTest(){

		//merge
		when(spiedList.size()).thenReturn(10);
		System.out.println("size of spiedlist after mocking size() method :" +spiedList.size());

		//when calling get(0) on spiedList real method is called . the list is EMPTY
		//when(spiedList.get(0)).thenReturn("element 1");
		//you have to use doReturn
		doReturn("element 1").when(spiedList).get(0);
		System.out.println("first element of spiedList after mocking get(0) method :"+spiedList.get(0));


	}

	@Test
	public void DoNothingTest() {

		//make clear() do nothing
		doNothing().when(spiedList).clear();

		spiedList.add("one");
		//clear() does nothing, so the list still contains "one"
		spiedList.clear();

		System.out.println("size of spiedList after doNothing on clear method() " + spiedList.size());
		assertEquals(1, spiedList.size());
	}

	@Test(expected = IllegalStateException.class)
	public void throwExceptionWhenCallingMethodTest() {
		when(mockedList.add(anyString())).thenThrow(IllegalStateException.class);
		mockedList.add("random string");
	}

}
