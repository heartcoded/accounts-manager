package com.diana.springboot.accountsmanager.learning;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import static org.junit.Assert.*;

import java.util.List;

import static org.mockito.Mockito.*;

public class VerifyMockitoTests {


	@Mock
	List<String> mockedList;

	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
	}
	@Test
	public void verifyCalledMethodsTest() {


		//once created a mock will remember ALL interactions (all methods applied to it)
		//then you can selectively verify whatever interactions you are interested in

		//using mock object
		mockedList.add("first element");
		mockedList.add("second element");
		mockedList.size();
		mockedList.clear();

		//verification - verify methods in any order you want
		//org.mockito.exceptions.verification.TooManyActualInvocations daca am fi apelat de mai multe ori ;
		//verify verifica ca s-a apelat O SINGURA DATA , daca nu specificam parametrii de tip times(n)
		verify(mockedList,description("this is called more than once")).add("second element");
		verify(mockedList).clear();
		verify(mockedList).add("first element");
		verify(mockedList).size();

	}

	@Test
	public void verifyNoOfTimesCalledMethodsTest() {

		//once created a mock will remember ALL interactions (all methods applied to it)
		//then you can selectively verify whatever interactions you are interested in


		//using mock object
		mockedList.add("first element");
		mockedList.add("second element");
		mockedList.size();
		mockedList.clear();

		//verifica daca metoda size() pe mockedobject s-a apelat o data.
		// description() = will print a custom message on verification failure (odata cu mockito-core 2.1.0 )
		verify(mockedList,times(1).description("this is called more than once")).size();
		verify(mockedList,times(1)).clear();
		verify(mockedList,times(2)).add(anyString());
	}

	@Test
	public void verifyNoInteractionWithMockTest() {
		verifyZeroInteractions(mockedList);
	}

	@Test
	public void verifyOrderOfInteractionTest() {
		mockedList.size();
		mockedList.add("a parameter");
		mockedList.clear();

		InOrder inOrder = Mockito.inOrder(mockedList);
		inOrder.verify(mockedList).size();
		inOrder.verify(mockedList).add("a parameter");
		inOrder.verify(mockedList).clear();
		inOrder.verifyNoMoreInteractions();
	}

	@Test
	public void verifyInteractionHasNotOccurredTest() {
		mockedList.size();
		verify(mockedList, never()).clear();
	}
	@Test
	public void verifyInteractionHasOccurredAtLeastCertainNoOfTimesTest() {
		mockedList.clear();
		mockedList.clear();
		mockedList.clear();

		verify(mockedList, atLeast(1)).clear();
		verify(mockedList, atMost(10)).clear();
	}

	@Test
	public  void resetMockTest(){

		when(mockedList.size()).thenReturn(10);
		//at this point the mock forgot any interactions & stubbing
		reset(mockedList);
		assertEquals(0, mockedList.size());

	}


}
