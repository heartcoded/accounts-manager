package com.diana.springboot.accountsmanager.repo;


import com.diana.springboot.accountsmanager.model.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.*;

import java.util.List;
import java.util.Optional;


/**
 * testing jpa entities & persistence layer
 * @DataJpaTest - will autoconfigure in-memory embedded databases and scan for @Entity classes and Spring Data JPA repositories.
 * The @DataJpaTest annotation doesn’t load other Spring beans (@Components, @Controller, @Service, and annotated beans) into ApplicationContext.
 * by default all tests are transactional and are rolled back when finished.
 *
 *as a dependency we have h2 (in memory database)
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountsRepositoryTests {

    //because @DataJpaTest load JPA repos into ApplicationContext. That's why we can use @Autowired
    @Autowired
    private AccountsRepository accountsRepository;

    @Test
    public void saveAccountTest() {
        Account account = new Account(146L,3000,1000);
        accountsRepository.save(account);
        Optional<Account> existingAccount = accountsRepository.findById(146L);
        assertTrue(existingAccount.isPresent());
        assertNotNull(existingAccount);
        assertEquals(account.getBalance(),existingAccount.get().getBalance(),0.1);
        assertEquals(account.getWithdrawalLimit(),existingAccount.get().getWithdrawalLimit(),0.1);

        //at the end of the test -> Rolled back transaction.
    }

    @Test
    public void getAllAccountsTest(){
        // spring will look into data.sql file
      /*  Account account1 = new Account(147L,3000,1000);
        Account account2 = new Account(148L,4000,2000);
        accountsRepository.save(account1);
        accountsRepository.save(account2);
*/
        List<Account> accounts = accountsRepository.findAll();
        for(Account a: accounts){
            System.out.println(a.toString());
        }
        assertEquals(3,accounts.size());

    }


    @Test
    public void findByIdNotFoundTest(){
       Optional<Account> account = accountsRepository.findById(329L);
       assertFalse(account.isPresent());

    }

}
