package com.diana.springboot.accountsmanager.controller;

import com.diana.springboot.accountsmanager.model.Account;
import com.diana.springboot.accountsmanager.service.IAccountManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @WebMvcTest http req/responses are mocked; the real connections are not created (the behavior when using @SpringBootTest)
 * @WebMvcTest is only going to scan our controller we've been defined and the MVC infrastructure. Controller's dependencies will be mocked.
 * This is much faster as we only load  tiny portion of our app.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(AccountsManagerController.class)
public class AccountsManagerControllerTests {

    //we have to mock our dependencies even though we don't use them.
    //'Error creating bean with name 'accountsManagerController': Unsatisfied dependency expressed through field 'accountManager''
    // using @Mock is not enough !
    // use @MockBean : a spring boot class. It allows to add Mockito mocks in a Spring ApplicationContext!!!
    @MockBean
    private IAccountManager accountManager;
    @Autowired
    private MockMvc mvc;

    @Test
    public void getAllAccountsTest() throws Exception {
        Account a1 = new Account(123L,400,100);
        Account a2 = new Account(124L,600,200);
        List<Account> accounts = new ArrayList<>();
        accounts.add(a1);
        accounts.add(a2);
        when(accountManager.getAccounts()).thenReturn(accounts);
        mvc.perform(MockMvcRequestBuilders.get("/accounts")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andDo(print());
    }

    @Test
    public void getAccountByIdTest() throws Exception {
        Account a1 = new Account(123L,400,100);
        when(accountManager.getAccountByIban(123L)).thenReturn(a1);
        mvc.perform(MockMvcRequestBuilders.get("/accounts/123").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.iban").exists())
                .andExpect(jsonPath("$.balance").exists())
                .andExpect(jsonPath("$.withdrawalLimit").exists())
                .andExpect(jsonPath("$.balance").value(400))
                .andExpect(jsonPath("$.withdrawalLimit").value(100))
                .andExpect(jsonPath("$.*", hasSize(3)))
                .andDo(print());
    }
}

/*
To test size of array: jsonPath("$", hasSize(4))

To count members of object: jsonPath("$.*", hasSize(4))
 */